<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('articles', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('description',1500);
        $table->float('price')->unsigned();
        $table->integer('total_in_shelf')->unsigned();
        $table->integer('total_in_vault')->unsigned();
        $table->integer('store_id')->unsigned();
        $table->timestamps();
        $table->foreign('store_id')
          ->references('id')
          ->on('stores')
          ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
