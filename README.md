# Super Zapateria

## Getting Started


### Clone the `super_zapateria` Repository

Clone the repository in a directory of your choosing:

```
git clone https://JoseRojas20@bitbucket.org/JoseRojas20/super_zapateria.git
```

### Run Your Local Environment

1. `cd` into your local `super_zapateria` repository
2. Run `composer update`.  
3. Run `npm install`.
4. Create a copy of .env.example named .env
4. Set the database variables in your env file.
5. Run laravel migration: `php artisan migrate`
6. Run laravel seeders: `php artisan db:seed`
7. Run your proyect: `php artisan key:generate`
7. Run your proyect: `php artisan serve`


## Api Routes

### Basic Authentication

| User          | Password      |
| ------------- |:-------------:|
| my_user       | my_password   |

### List

1. /services/articles : Get a json with all articles.
2. /services/stores : Get a json with all stores.
3. /services/stores/{store_id}/articles : Get a jason with all the Store's articles.

## Run Unit Tests

1. `cd` into your local `super_zapateria` repository
2. Run `vendor/bin/phpunit`

## User Interface Routes

### Stores
1. View All : /admin/store
2. Create: /admin/store/create
3. Edit: /admin/store/{id}/edit

### Articles
1. View All : /admin/article
2. Create: /admin/article/create
3. Edit: /admin/article/{id}/edi

## Author

José Angel Rojas Solera
