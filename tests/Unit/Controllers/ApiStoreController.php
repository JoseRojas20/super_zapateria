<?php

namespace Tests\Unit\Controller;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiStoreController extends TestCase
{
  /**
   * Get User Authentication Info
   *
   * @return Array
   */
   public function getUserAuth(){
    return ['PHP_AUTH_USER'=>'my_user', 'PHP_AUTH_PW'=>'my_password'];
   }

    /**
     * Unit Test for Auth Api Endpoint
     *
     * @return void
     */
    public function testFetchStoresAuthFail()
    {
      $result = $this->json('GET', '/services/stores');
      $data = json_decode($result->getContent(),TRUE);
      $this->assertEquals(false, $data['success']);
      $this->assertEquals(401, $data['error_code']);
    }

    /**
     * Unit Test for Stores Api Endpoint
     *
     * @return void
     */
    public function testFetchStores()
    {
      $result = $this->json('GET','/services/stores',
        [], $this->getUserAuth());
      $data = json_decode($result->getContent(),TRUE);
      $this->assertEquals(true, $data['success']);
      $this->assertInternalType('array', $data['stores']);
    }

    /**
     * Unit Test for Stores' Articles Api Endpoint
     *
     * @return void
     */
    public function testFetchStoresArticles()
    {
      $result = $this->json('GET','/services/stores/10/articles',
        [], $this->getUserAuth());
      $data = json_decode($result->getContent(),TRUE);
      $this->assertEquals(true, $data['success']);
      $this->assertInternalType('array', $data['articles']);
    }

    /**
     * Unit Test for Stores' Articles Api Endpoint with
     * wrong parameters
     *
     * @return void
     */
    public function testFetchStoresArticlesBadRequest()
    {
      $result = $this->json('GET','/services/stores/test/articles',
        [], $this->getUserAuth());
      $data = json_decode($result->getContent(),TRUE);
      $this->assertEquals(false, $data['success']);
      $this->assertEquals(400, $data['error_code']);
    }

    /**
     * Unit Test for Stores' Articles Api Endpoint
     * with a not existing ID
     *
     * @return void
     */
    public function testFetchStoresArticlesNotFound()
    {
      $result = $this->json('GET', '/services/stores/1250/articles',
        [], $this->getUserAuth());
      $data = json_decode($result->getContent(),TRUE);
      $this->assertEquals(false, $data['success']);
      $this->assertEquals(404, $data['error_code']);
    }

}
