<?php

namespace Tests\Unit\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ApiArticleController extends TestCase
{
  /**
   * Get User Authentication Info
   *
   * @return Array
   */
   public function getUserAuth(){
    return ['PHP_AUTH_USER'=>'my_user', 'PHP_AUTH_PW'=>'my_password'];
   }

  /**
   * Unit Test for Auth Api Endpoint
   *
   * @return void
   */
    public function testFetchArticlesAuthFail()
    {
      $result = $this->json('GET', '/services/articles');
      $data = json_decode($result->getContent(),TRUE);
      $this->assertEquals(false, $data['success']);
      $this->assertEquals(401, $data['error_code']);
    }

    public function testFetchArticles()
    {
      $result = $this->json('GET','/services/articles',
        [], $this->getUserAuth());
      $data = json_decode($result->getContent(),TRUE);
      $this->assertEquals(true, $data['success']);
      $this->assertInternalType('array', $data['articles']);
    }
}
