<?php

namespace App\Http\Middleware;

use Closure;

class ApiBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if ($request->header('PHP_AUTH_USER') != 'my_user'
            && $request->header('PHP_AUTH_PW') != "my_password") {
            $data = ['success'=>false, 'error_msg' => 'Not authorized', 'error_code' => 401];
            return response()->json($data, 401, array(), JSON_PRETTY_PRINT);
        }
        return $next($request);
    }
}
