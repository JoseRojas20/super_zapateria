<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Store;

class ApiStoreController extends Controller
{
    /**
     * Return a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $stores = Store::all();
            return [
              'stores' => $stores,
              'success' => true,
              'total_elements' => $stores->count()
            ];
        }
        catch(\Exception $e)
        {
            return [
              'success' => false
            ];
        }
    }

    /**
     * Return listing with the Articles
     * of the specific Store.
     *
     * @return \Illuminate\Http\Response
     */
      public function articles($id)
      {
        try
        {
          if(is_numeric($id)){
            $store = Store::with('articles')->findOrFail($id);
            return [
              'articles' => $store->articles,
              'success' => true,
              'total_elements' => $store->articles->count()
            ];
          } else {
            return [
               'success' => false,
               'error_msg' => 'Bad Request',
               'error_code' => 400
            ];
          }
        }
        catch (ModelNotFoundException $ex) {
          return [
             'success' => false,
             'error_msg' => 'Record not Found',
             'error_code' => 404
          ];
        }
      }
}
