<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ApiArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $articles = Article::all();
            return [
              'articles' => $articles,
              'success' => true,
              'total_elements' => $articles->count()
            ];
        }
        catch(Exception $e)
        {
            return [
              'success' => false
            ];
        }
    }
}
