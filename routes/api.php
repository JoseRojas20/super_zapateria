<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'apiAuth'], function () {
  Route::get('articles', 'ApiArticleController@index');
  Route::get('stores', 'ApiStoreController@index');
  Route::get('stores/{id}/articles', 'ApiStoreController@articles');
});
