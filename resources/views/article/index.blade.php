@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <h2>
                        All Articles
                        <a class="btn btn-info pull-right" href="/admin/article/create">Create New Article</a>
                      </h2>
                    </div>
                    <div class="panel-body">
                        <span>Number of Articles in list: {{ count($articles) }}</span>
                        <div class="table-responsive">
                            <table id="article-table" class="table table-borderless table-striped">
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> Price </th>
                                        <th> Total in Shelf </th>
                                        <th> Total in Vault </th>
                                        <th> Store </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->total_in_shelf }}</td>
                                        <td>{{ $item->total_in_vault }}</td>
                                        <td>{{ $stores[$item->store_id]? $stores[$item->store_id] : $item->store_id }}</td>
                                        <td>
                                            <a href="{{ url('/admin/article/' . $item->id) }}" class="btn btn-success btn-xs" title="View Article"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/admin/article/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Article"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/article', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete District" />', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete Article',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $articles->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
