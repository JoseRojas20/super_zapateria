@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Article: {{ $article->name }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/article/' . $article->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Store"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/article', $article->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Article',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $article->id }}</td>
                                    </tr>
                                    <tr>
                                      <th> Name </th>
                                      <td> {{ $article->name }} </td>
                                    </tr>
                                    <tr>
                                      <th> Description </th>
                                      <td> {{ $article->description }} </td>
                                    </tr>
                                    <tr>
                                      <th> Price </th>
                                      <td> {{ $article->price }} </td>
                                    </tr>
                                    <tr>
                                      <th> Total in Shelf </th>
                                      <td> {{ $article->total_in_shelf }} </td>
                                    </tr>
                                    <tr>
                                      <th> Total in Vault </th>
                                      <td> {{ $article->total_in_vault }} </td>
                                    </tr>
                                    <tr>
                                      <th> Store </th>
                                      <td> {{ $article->store_id }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
