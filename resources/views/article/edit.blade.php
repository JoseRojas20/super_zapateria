@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Article {{ $article->name }}</div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($article, [
                            'method' => 'PATCH',
                            'url' => ['/admin/article', $article->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('article.form', ['submitButtonText' => 'Update','stores' => $stores])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
