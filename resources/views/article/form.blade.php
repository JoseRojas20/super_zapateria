<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name*', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required'=>'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control','rows'=>3]) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price*', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('price', null, ['class' => 'form-control','step'=>'any','required'=>'required']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('total_in_shelf') ? 'has-error' : ''}}">
    {!! Form::label('total_in_shelf', 'Total in Shelf', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('total_in_shelf', null, ['class' => 'form-control']) !!}
        {!! $errors->first('total_in_shelf', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('total_in_vault') ? 'has-error' : ''}}">
    {!! Form::label('total_in_vault', 'Total in Vault', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('total_in_vault', null, ['class' => 'form-control']) !!}
        {!! $errors->first('total_in_vault', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($stores))
  <div class="form-group {{ $errors->has('store_id') ? 'has-error' : ''}}">
      {!! Form::label('store_id', 'Store*', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::select('store_id', $stores, null, ['class' => 'form-control', 'required'=>'required']) !!}
          {!! $errors->first('store_id', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
@endif



<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
