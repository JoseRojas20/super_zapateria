@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <h2>
                        All Stores
                        <a class="btn btn-info pull-right" href="/admin/store/create">Create New Store</a>
                      </h2>
                    </div>
                    <div class="panel-body">
                        <span>Number of Stores in list: {{ count($stores) }}</span>
                        <div class="table-responsive">
                            <table id="store-table" class="table table-borderless table-striped">
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> Address </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($stores as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->address }}</td>
                                        <td>
                                            <a href="{{ url('/admin/store/' . $item->id) }}" class="btn btn-success btn-xs" title="View Store"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/admin/store/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Stores"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/store', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete District" />', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete District',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $stores->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
